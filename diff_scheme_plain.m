% Курсовая работа
% Расчет малых поперечных колебаний тонкой однородной струны с жетско закреплеными концами x = 0 и x = l, если струна возбуждена начальным отклонением ksi
%% 
%Объявляем входные данные:
T = 20;      % расчет на промежутке времени 0 < t <= T
l = 10;      % расчет на пространственном промежутке 0 <= x < l
I = 160;      % количество отрезков разбиения пространства x
K = 320;      % количество отрезков разбиения времени t

%% 
% Инициализируем параметры:
psi_func = @(x) -16/ (3 * l^2) * (x.^2-x*l); % начальное отклонение
eta = 1;    % величина натяжения струны
rho = 1;    % линейная плотность струны
a = sqrt(eta/rho);

% Инициализиуем сетку и константы:
t = linspace(0, T, K + 1);
x = linspace(0, l, I + 1);
h_t = T / K;
h_x = l / I;
gamma_const = (a^2 * h_t^2) / h_x^2;
psi = psi_func(x);

% Явная конечно-разностная схема первого порядка:
U = nan([I+1, K+1]);    % U(x, t)

U(1, :) = 0;            % краевые условия
U(end, :) = 0;
U(2:end-1, 1) = psi(2:end-1);   % начальные условия
U(2:end-1, 2) = U(2:end-1, 1);
%U(2:end-1, 2) = eta * h_t + U(2:end-1, 1); % ВОТ ЭТО ВСЁ ПОРТИТ!!!!!

% U(1, 3:end) = 0;            % краевые условия
% U(end, 3:end) = 0;
% U(1:end, 1) = psi(1:end);   % начальные условия
% U(1:end, 2) = eta * h_t + U(1:end, 1);

% Рекуррентно вычислим U^k+1_i для первого порядка точности:
for kk = (2:K)  % цикл по времени
    U(2:end-1, kk + 1) = 2 * U(2:end-1, kk) - U(2:end-1, kk-1) + gamma_const * (U(3:end, kk) - 2 * U(2:end-1, kk) + U(1:end-2, kk));% + h_t^2 * rho;
end

% Пострим график для первого порядка точности:
%U
disp("Построим графики для первого порядка точности:")
plot(x, U(:,1));
title("Отклонение струны в начальный момент времени")
xlabel("x")
ylabel("U(x, t)")
plot(x, U(:,end));
title("Отклонение струны в конечный момент времени")
xlabel("x")
ylabel("U(x, t)")

figure(2);
for kk = (1:K+1)   % цикл по времени
    plot(x, U(:, kk))
    ylim([-1.4, 1.4]);
    %ylim([-10, 10]);
    drawnow;
    pause(0.01) %in seconds
end

% Явная конечно-разностная схема второго порядка точности:
U = nan([I+1, K+1]);    % X x T

U(1, :) = 0;            % U(x, t)
U(end, :) = 0;
U(2:end-1, 1) = psi(2:end-1);
U(2:end-1, 2) = eta * h_t + U(2:end-1, 1) + h_t^2 / 2 * (a^2*(psi(3:end) - 2 * psi(2:end-1) + psi(1:end-2))'/h_x^2 + rho);

% Рекуррентно вычислим U^k+1_i для второго порядка точности:
for kk = (2:length(t)-1)  % цикл по времени, изменить length(t) на K
    U(2:end-1, kk + 1) = 2 * U(2:end-1, kk) - U(2:end-1, kk-1) + gamma_const * (U(3:end, kk) - 2 * U(2:end-1, kk) + U(1:end-2, kk)) + h_t^2 * rho;
end

% Пострим график для второго порядка точности:
disp("Построим графики для второго порядка точности:")
plot(x, U(:,1));
title("Отклонение струны в начальный момент времени")
xlabel("x")
ylabel("U(x, t)")
plot(x, U(:,end));
title("Отклонение струны в конечный момент времени")
xlabel("x")
ylabel("U(x, t)")

% Вычислим аналитическое решение:
n = 100;    % количество членов ряда;

N = (1:n);
lambda = ((pi * N * a) / l).^2;
betta_integrand = @(x_inner) psi_func(x_inner) * sin(pi * N * x_inner / l);
%betta = 2./(pi * N * a).* integral(betta_integrand, 0, l, 'ArrayValued', true)
betta = 2/l * integral(betta_integrand, 0, l, 'ArrayValued', true)
alpha_integrand = @(x_inner) eta * sin(pi * N * x_inner / l)
%alpha = 2./l.* integral(alpha_integrand, 0, l, 'ArrayValued', true)
alpha = 2./(pi * N * a).* integral(alpha_integrand, 0, l, 'ArrayValued', true)

U_analytic = nan([I+1, K+1]);
for kk = (1:K+1)   % цикл по времени
    for ii = (1:I+1)
        U_analytic(ii, kk) = sum((alpha.*sin(sqrt(lambda).* t(kk)) + betta.* cos(sqrt(lambda).* t(kk))).* sin(pi * N * x(ii) / l));
    end
end
% 
% figure(3);
% for kk = (1:K+1)   % цикл по времени
%     plot(x, U_analytic(:, kk))
%     %ylim([-1.4, 1.4]);
%     ylim([-6, 6]);
%     drawnow;
%     pause(0.01) %in seconds
% end


